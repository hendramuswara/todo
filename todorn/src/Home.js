import React, { Component } from 'react'
import { Text, ImageBackground ,View } from 'react-native'

export default class Home extends Component {
    render() {
        return (

            <ImageBackground source={require('./bgimage.jpg')} style={{ width: '100%', height: '100%' }}>
               <View
                style={{
                    width: '100%', 
                    height: '100%',
                    marginTop : 40
                }}>
            
                <Text
                    style={{

                        fontSize: 50,
                        color: 'white',
                        fontWeight: 'bold',
                        alignSelf: 'center'
                        


                    }}

                > Welcome </Text></View></ImageBackground>
      

        )
    }
}
