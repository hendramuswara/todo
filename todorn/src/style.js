import {StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    button : {
        
        borderColor : 'white',
        borderRadius : 20,
        borderWidth: 2,
        display : 'flex',
        width: '50%',
        justifyContent: 'center',
        alignSelf:'center',
        backgroundColor : 'transparent',
        color : '#ffff',
        marginTop : 20
    },
    button2 : {
        
        borderColor : 'white',
        borderRadius : 20,
        borderWidth: 2,
        display : 'flex',
        width: '50%',
        justifyContent: 'center',
        alignSelf:'center',
        backgroundColor : 'transparent',
        color : '#ffff',
   
    },
    button3 : {
        
        borderColor : 'white',
        borderRadius : 20,
        borderWidth: 2,
        display : 'flex',
        width: '50%',
        justifyContent: 'center',
        alignSelf:'center',
        backgroundColor : 'transparent',
        color : '#ffff',

    },

    isComplete :{
        color: 'white', fontSize: 16, fontWeight: 'bold',
       textDecorationLine: 'line-through'
    },

    isComs : {
        color: 'white', fontSize: 16, fontWeight: 'bold',
    },

    input :{
        color : 'white' ,
         fontSize : 20 , 
         fontWeight : 'bold' ,
          borderWidth:2 ,
         borderRadius : 20,
         borderColor : 'white',
         marginTop: 40
    }
})
export default styles