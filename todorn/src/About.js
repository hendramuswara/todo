import React, { Component } from 'react'
import { Text, View, ImageBackground } from 'react-native'

export default class About extends Component {
    render() {
        return (
           
            <ImageBackground source={require('./bgimage.jpg')} style={{ width: '100%', height: '100%' }}>
            <View
             style={{
                 width: '100%', 
                 height: '100%',
                 marginTop : 40
             }}>
         
             <Text
                 style={{

                     fontSize: 50,
                     color: 'white',
                     fontWeight: 'bold',
                     alignSelf: 'center'
                     


                 }}

             > About </Text></View></ImageBackground>
   
   
        )
    }
}
