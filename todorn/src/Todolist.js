import React, { Component } from 'react'
import { View, Modal, Text, Alert, ImageBackground } from 'react-native'
import Axios from 'axios';
import { Input, Container, Content, Card, CardItem, Button, Item, Left, CheckBox, Icon } from 'native-base';
import styles from './style'


export default class Todolist extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            judul: '',
            _id: '',
            state: '',
            id: '',
            modalVisible: false,
            modalVisibles: false,
            isChecked: false

        }

    }

    componentDidMount = () => {
        this.datas()
        setInterval(this.datas, 1000)
        // console.log('props data',this.props.data)
    }



    datas = () => {
        Axios.get(`https://btm-rn.herokuapp.com/api/v1/todo`)
            .then(res => {
                this.setState({
                    data: res.data.results
                })
            })
    }


    inputData = () => {
        const tambah = async objParam => await Axios.post(
            `https://btm-rn.herokuapp.com/api/v1/todo`, objParam
        )

        tambah({
            title: this.state.judul
        })
            .then(res => {
                console.log(res.data)
            })
            .catch(e => console.log(e))

        this.datas();
    }

    editData = async (id, objParam) => {
        await Axios.put(
            `https://btm-rn.herokuapp.com/api/v1/todo/${id}`, objParam
        )
    }

    deleteData = async id => await Axios.delete(
        `https://btm-rn.herokuapp.com/api/v1/todo/${id}`
    )

    testrue = (isCom) => {
        if (isCom === true) {
            return false
        } else if (isCom === false) {
            return true
        } else {
            console.log(`Jangan kesini Sayang :(`)
        }
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible })
    }

    getModalVisible = (visible) => {
        this.setState({ modalVisibles: visible })
    }

    toggle(id, status, title) {
        this.setState({
            id: id
        })
        this.setState({
            state: status
        })
        this.setState({
            judul: title
        })
    }



    render() {

        const list = this.state.data.map(hasil => {
            console.log(hasil)
            return (
                <>
                    <Card >
                        <CardItem style={{
                            backgroundColor: '#34495E', display: 'flex', justifyContent: 'center',
                            alignSelf: 'center', width: '100%'
                        }} key={hasil._id}>

                        <Icon type="AntDesign" name="checkcircleo"
                             checked={this.state.isChecked}
                                onPress={
                                    () => {

                                        this.editData(
                                            hasil._id,
                                            {
                                                title: hasil.title,
                                                isComplete: this.testrue(hasil.isComplete)
                                            }
                                        )

                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        })
                                    }} />
                            <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold',
                            textDecorationLine :hasil.isComplete ? 'line-through' : 'none'  
                            }}>{hasil.title}</Text>
                        </CardItem>
                        <CardItem style={{ backgroundColor: '#1B2631' }} >
                            <Button style={styles.button2} onPress={
                                () => this.deleteData(hasil._id)}><Text style={{ color: 'white', fontSize: 16 }}>
                                    Hapus
                                    </Text>
                            </Button>

                            <Button style={styles.button3} onPress={
                                () => {
                                    this.toggle(hasil._id, hasil.isComplete, hasil.title)
                                    this.getModalVisible(true)
                                }}><Text style={{ color: 'white', fontSize: 16 }}>Edit</Text></Button>
                        </CardItem>
                    </Card>

                </>
            )
        });



        return (

            <Container>
                <ImageBackground source={require('./bgimage.jpg')} style={{ width: '100%', height: '100%' }}>
                    <Content>

                        <Button style={styles.button} title="Tambah Data" type="outline"
                            onPress={() => {
                                this.setModalVisible(true);
                            }}><Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}> Tambah Data </Text>
                        </Button>

                        {list}
                    </Content>
                </ImageBackground>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}

                >

                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <View style={{
                            backgroundColor: '#34495E', width: '90%', height: '80%'

                        }}>

                            <Item style={{ borderColor: 'white', borderWidth: 2 }}>
                                <Input style={styles.input} valid placeholder="Input Disini yaa" onChangeText={(text) => this.setState({ judul: text })} />
                            </Item>
                            <Button style={styles.button}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                    this.inputData()
                                }}>
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Tambah Data</Text>
                            </Button>
                            <Button style={styles.button}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Cancel</Text>
                            </Button>
                        </View>

                    </View>
                </Modal>

                {/* ACTION EDIT */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisibles}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <View style={{ backgroundColor: '#34495E', width: '90%', height: '80%' }}>

                            <Item style={{ borderColor: 'white', borderWidth: 2 }}>
                                <Input style={styles.input} valid onChangeText={(text) => this.setState({ judul: text })} value={this.state.judul} /></Item>
                            <Button style={styles.button}
                                onPress={() => {

                                    this.getModalVisible(!this.state.modalVisibles)
                                    this.editData(
                                        this.state.id,
                                        {
                                            title: this.state.judul,
                                            isComplete: this.state.state
                                        }
                                    )
                                }} >
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Update</Text>
                            </Button>
                            <Button style={styles.button}
                                onPress={() => {
                                    this.getModalVisible(!this.state.modalVisibles);
                                }}>
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Cancel</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </Container>

        )
    }
}
