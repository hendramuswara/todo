import React, { Component } from 'react'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import {
  createBottomTabNavigator,
  createAppContainer,
} from 'react-navigation'

import {Icon} from 'native-base'
import Todolist from './src/Todolist'
import Home from './src/Home'
import About from './src/About'


class homeScreen extends React.Component {
  render() {
    return  <Home />
     
    
  }
}

class todoScreen extends React.Component {
  render() {
    return        <Todolist />

    
  }
}
class aboutScreen extends React.Component {
  render() {
    return  <About />
      
    
  }
}

const Apps = createMaterialBottomTabNavigator (
  {
    Home:  {screen : homeScreen,
      navigationOptions: {
        tabBarLabel:"Home Page",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={30} color="#fff" />
        )
      },
    },
    Todolist : {screen : todoScreen,
      navigationOptions: {
        tabBarLabel:"Todolist",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="list" size={30} color="#fff" />
        )
      },
    },
    About : {screen: aboutScreen,
      navigationOptions: {
        tabBarLabel:"About",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="person" size={30} color="#fff" />
        )
      },
    }
  },
  {
    
    initialRouteName: 'Home',
    activeColor: '#f0edf6',
    inactiveColor: '#ffff',
    barStyle: { backgroundColor: '#222f3e' }
  });


const AppContainer = createAppContainer(Apps)

export default class App extends Component {
  render() {
    return <AppContainer />
      
    
  }
}
